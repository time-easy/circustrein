﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace CircustreinApplicatie
{
    class Train
    {
        public List<Wagon> Wagons { get; private set; }

        public Train()
        {
            this.Wagons = new List<Wagon>();
        }

        public void AddAnimal(Animal animal)
        {
            foreach (Wagon wagon in this.Wagons)
            {
                if (wagon.IsEligible(animal))
                {
                    wagon.Animals.Add(animal);
                    return;
                }
            }
            this.Wagons.Add(new Wagon(animal));
        }

    }
}
