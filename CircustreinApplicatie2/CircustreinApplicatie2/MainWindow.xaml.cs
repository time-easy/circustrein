﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using CircustreinApplicatie;

namespace CircustreinApplicatie2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private List<Animal> _animals = new List<Animal>
        {
            new Animal("Tijger", Weight.Medium, Diet.Carnivore),
            new Animal("Walvis", Weight.Large, Diet.Herbivore),
            new Animal("Konijn", Weight.Small, Diet.Herbivore),
            new Animal("Haai", Weight.Large, Diet.Carnivore),
            new Animal("Vliegtuig", Weight.Large, Diet.Carnivore),
            new Animal("Vis", Weight.Small, Diet.Herbivore),
            new Animal("Hond", Weight.Medium, Diet.Carnivore),
            new Animal("Paard", Weight.Medium, Diet.Herbivore),
            new Animal("Kat", Weight.Small, Diet.Carnivore),
            new Animal("Muis", Weight.Small, Diet.Herbivore),
            new Animal("Olifant", Weight.Large, Diet.Herbivore),
            new Animal("Dingo", Weight.Medium, Diet.Carnivore),
            new Animal("Giraffe", Weight.Large, Diet.Herbivore),
            new Animal("Rat", Weight.Small, Diet.Herbivore),
            new Animal("Nijlpaard", Weight.Large, Diet.Carnivore),
            new Animal("Mewtwo", Weight.Large, Diet.Carnivore),
            new Animal("Lieveheersbeestje", Weight.Small, Diet.Herbivore),
            new Animal("Kont", Weight.Medium, Diet.Carnivore),
            new Animal("Eenhoorn", Weight.Medium, Diet.Herbivore),
            new Animal("Whiteboardmarker", Weight.Small, Diet.Carnivore),
            new Animal("Mug", Weight.Small, Diet.Herbivore),
            new Animal("Kameel", Weight.Large, Diet.Herbivore),
        };

        public MainWindow()
        {
            InitializeComponent();
            lbAnimals.ItemsSource = _animals;
        }

        private void BtnPlace_OnClick(object sender, RoutedEventArgs e)
        {
            Train train = new Train();
            IEnumerable<Animal> carnivores = _animals
                .Where(a => a.Diet == Diet.Carnivore);
            List<Animal> herbivores = _animals
                .Where(animal => animal.Diet == Diet.Herbivore)
                .OrderBy(animal => animal.Weight)
                .ToList();
            foreach (Animal carnivore in carnivores)
            {
                Wagon wagon = new Wagon();
                wagon.Animals.Add(carnivore);
                for (int i = herbivores.Count - 1; i >= 0; i--)
                {
                    if (wagon.IsEligible(herbivores[i]))
                    {
                        wagon.Animals.Add(herbivores[i]);
                        herbivores.RemoveAt(i);
                    }
                }
                train.Wagons.Add(wagon);
            }

            foreach (Animal herbivore in herbivores)
            {
                train.AddAnimal(herbivore);
            }

            lbWagons.ItemsSource = train.Wagons;
            MessageBox.Show(train.Wagons.Count + " wagons gegenereerd!");
        }

        private void lbWagons_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            Wagon wagon = (Wagon)e.AddedItems[0];
            lbWagonAnimals.ItemsSource = wagon.Animals;
        }
    }
}