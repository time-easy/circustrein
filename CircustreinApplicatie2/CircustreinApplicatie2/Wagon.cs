﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CircustreinApplicatie
{
    class Wagon
    {
        public List<Animal> Animals { get; private set; }

        public int ContainingWeight => this.Animals.Sum(animal => (int)animal.Weight);

        private const int maxWeight = 10;
        public int MaxWeight => maxWeight;

        public Wagon()
        {
            this.Animals = new List<Animal>();
        }

        public Wagon(Animal firstAnimal)
        {
            this.Animals = new List<Animal>
            {
                firstAnimal
            };
        }

        public bool IsEligible(Animal animal)
        {
            //Does the animal fit in the Wagon?
            if ((int)animal.Weight > this.MaxWeight - this.ContainingWeight)
                return false;

            //Will the animal get eaten?
            IEnumerable<Animal> carnivoresInWagon = this.Animals.Where(a => a.Diet == Diet.Carnivore);
            foreach (Animal carnivore in carnivoresInWagon)
            {
                if (animal.Weight <= carnivore.Weight)
                    return false;
            }

            return true;
        }

        public override string ToString()
        {
            return this.Animals.Count.ToString() + " dieren, gewicht:" + this.ContainingWeight;
        }
    }
}
