﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CircustreinApplicatie
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Animal> _animals = new List<Animal>
        {
            new Animal("Tijger", Weight.Medium, Diet.Carnivore),
            new Animal("Walvis", Weight.Large, Diet.Herbivore),
            new Animal("Konijn", Weight.Small, Diet.Herbivore),
            new Animal("Haai", Weight.Large, Diet.Carnivore),
            new Animal("Vliegtuig", Weight.Large, Diet.Carnivore),
            new Animal("Vis", Weight.Small, Diet.Herbivore),
            new Animal("Hond", Weight.Medium, Diet.Carnivore),
            new Animal("Paard", Weight.Medium, Diet.Herbivore),
            new Animal("Kat", Weight.Small, Diet.Carnivore),
            new Animal("Muis", Weight.Small, Diet.Herbivore),
            new Animal("Olifant", Weight.Large, Diet.Herbivore),
            new Animal("Dingo", Weight.Medium, Diet.Carnivore),
            new Animal("Giraffe", Weight.Large, Diet.Carnivore),
            new Animal("Rat", Weight.Small, Diet.Carnivore),
            new Animal("Nijlpaard", Weight.Large, Diet.Carnivore),
            new Animal("Mewtwo", Weight.Large, Diet.Carnivore),
            new Animal("Lieveheersbeestje", Weight.Small, Diet.Carnivore),
            new Animal("Kont", Weight.Medium, Diet.Carnivore),
            new Animal("Eenhoorn", Weight.Medium, Diet.Carnivore),
            new Animal("Whiteboardmarker", Weight.Small, Diet.Carnivore),
            new Animal("Mug", Weight.Small, Diet.Carnivore),
            new Animal("Kameel", Weight.Large, Diet.Carnivore),
        };
        public MainWindow()
        {
            InitializeComponent();
            lbAnimals.ItemsSource = _animals;
        }

        private void btnPlace_Click(object sender, RoutedEventArgs e)
        {
            Train train = new Train();

            foreach (Animal animal in this._animals)
            {
                train.AddToTrain(animal);
            }

            lbWagons.ItemsSource = train.Wagons;
        }

        private void lbWagons_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Wagon wagon = (Wagon)e.AddedItems[0];
            lbWagonAnimals.ItemsSource = wagon.Animals;
        }
    }
}
