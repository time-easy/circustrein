﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircustreinApplicatie
{
    class Wagon
    {
        public List<Animal> Animals { get; private set; }

        public int AnimalCount => this.Animals.Count;

        public int ContainingWeight => this.Animals.Sum(animal => (int)animal.Weight);

        private const int maxWeight = 10;

        public Wagon()
        {
            this.Animals = new List<Animal>();
        }

        public Wagon(List<Animal> animals)
        {
            Animals = animals;
        }

        public int MaxWeight => maxWeight;

        public bool ContainsDanger(Animal animal)
        {
            IEnumerable<Animal> dangerousAnimals =
                from a in this.Animals
                where a.Diet == Diet.Carnivore && a.Weight >= animal.Weight
                select a;
            return dangerousAnimals.Any();
        }

        public bool IsDangerous(Animal animal)
        {
            if (animal.Diet == Diet.Herbivore)
            {
                return false;
            }

            IEnumerable<Animal> dangeredAnimals =
                from a in this.Animals
                where a.Weight <= animal.Weight
                select a;
            return dangeredAnimals.Any();
        }
    }
}
