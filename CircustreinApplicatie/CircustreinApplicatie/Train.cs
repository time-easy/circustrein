﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace CircustreinApplicatie
{
    class Train
    {
        public List<Wagon> Wagons { get; private set; }

        public Train()
        {
            this.Wagons = new List<Wagon>();
        }

        public void AddToTrain(Animal animal)
        {
            IEnumerable<Wagon> eligibleWagons = EligibleWagons(animal).OrderByDescending(wagon => wagon.ContainingWeight);
            if (eligibleWagons.Any())
            {
                eligibleWagons.First().Animals.Add(animal);
            }
            else
            {
                this.Wagons.Add(new Wagon(
                    new List<Animal> {animal}
                ));
            }
        }

        private IEnumerable<Wagon> EligibleWagons(Animal animal)
        {
            return
                from wagon in Wagons
                where (wagon.MaxWeight - wagon.ContainingWeight) >= (int) animal.Weight
                && !wagon.ContainsDanger(animal)
                && !wagon.IsDangerous(animal)
                select wagon;
        }
    }
}
