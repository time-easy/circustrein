﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircustreinApplicatie
{
    class Animal
    {
        public string Name { get; private set; }
        public Weight Weight { get; private set; }
        public Diet Diet { get; private set; }

        public Animal(string name, Weight weight, Diet diet)
        {
            Name = name;
            Weight = weight;
            Diet = diet;
        }
    }
}
