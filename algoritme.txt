Maak een collectie van alle dieren. Gesorteerd op gewicht (DESC);
Groepeer de dieren op dieet;
if (staan er dieren in de collectie van carnivoren?) {
	Voeg het eerste dier uit de collectie van carnivoren toe aan een nieuwe wagon;
	Verwijder het eerste dier uit de collectie van carnivoren;
	Maak een collectie van herbivoren die nog niet in de trein staan;
	Sorteer de collectie met herbivoren die nog niet in de trein staan, gesorteerd op gewicht (DESC);
	while (staan er dieren in de collectie van herbivoren die nog niet in de trein staan?) {
		if (Past het laatste dier in de collectie van niet-toegevoegde herbivoren in de wagon?) {
			if (is het gewicht van het laatste dier in de collectie van niet-toegevoegde herbivoren groter dan het hoogste gewicht van de carnivoren in de wagon?) {
				Voeg het laatste dier uit de collectie van niet-toegevoegde dieren toe aan de wagon;	
			}
		} else {
			Verwijder het laatste dier uit de collectie met niet-toegevoegde dieren;
		}
	}
} else {
	Maak een collectie van dieren die nog niet zijn toegevoegd in de trein;
	while (staan er dieren in de collectie van niet-toegevoegde dieren) {
		if (Past het dier in een wagon waar het niet opgegeten zal worden?) {
			Voeg het dier toe aan die wagon;
		} else {
			Plaats het dier in een nieuwe wagon;
		}
		Verwijder het dier uit de collectie van niet-toegevoegde dieren;
	}
}
