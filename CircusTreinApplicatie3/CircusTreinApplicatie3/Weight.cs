﻿namespace CircustreinApplicatie
{
    public enum Weight
    {
        Small = 1,
        Medium = 3,
        Large = 5
    }
}