﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CircustreinApplicatie;

namespace CircusTreinApplicatie3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Animal> animals = new List<Animal>
            {
                new Animal("Tijger", Weight.Medium, Diet.Carnivore),
                new Animal("Walvis", Weight.Large, Diet.Herbivore),
                new Animal("Konijn", Weight.Small, Diet.Herbivore),
                new Animal("Haai", Weight.Large, Diet.Carnivore),
                new Animal("Vliegtuig", Weight.Large, Diet.Carnivore),
                new Animal("Vis", Weight.Small, Diet.Herbivore),
                new Animal("Hond", Weight.Medium, Diet.Carnivore),
                new Animal("Paard", Weight.Medium, Diet.Herbivore),
                new Animal("Kat", Weight.Small, Diet.Carnivore),
                new Animal("Muis", Weight.Small, Diet.Herbivore),
                new Animal("Olifant", Weight.Large, Diet.Herbivore),
                new Animal("Dingo", Weight.Medium, Diet.Carnivore),
                new Animal("Giraffe", Weight.Large, Diet.Herbivore),
                new Animal("Rat", Weight.Small, Diet.Herbivore),
                new Animal("Nijlpaard", Weight.Large, Diet.Carnivore),
                new Animal("Mewtwo", Weight.Large, Diet.Carnivore),
                new Animal("Lieveheersbeestje", Weight.Small, Diet.Herbivore),
                new Animal("Kont", Weight.Medium, Diet.Carnivore),
                new Animal("Eenhoorn", Weight.Medium, Diet.Herbivore),
                new Animal("Whiteboardmarker", Weight.Small, Diet.Carnivore),
                new Animal("Mug", Weight.Small, Diet.Herbivore),
                new Animal("Kameel", Weight.Large, Diet.Herbivore),
            };
            Train train = new Train();
            train.AddAnimals(animals);
            Console.Write(train);
            Console.ReadKey();
        }
    }
}
