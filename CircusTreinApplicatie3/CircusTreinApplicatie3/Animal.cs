﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircustreinApplicatie
{
    public class Animal
    {
        public string Name { get; set; }
        public Weight Weight { get; set; }
        public Diet Diet { get; set; }

        public Animal(string name, Weight weight, Diet diet)
        {
            Name = name;
            Weight = weight;
            Diet = diet;
        }

        public Animal()
        {

        }

        public override string ToString()
        {
            return this.Name + " (" + this.Diet + ") " + this.Weight;
        }
    }
}
