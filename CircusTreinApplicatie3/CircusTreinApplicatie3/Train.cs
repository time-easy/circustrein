﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace CircustreinApplicatie
{
    class Train
    {
        public List<Wagon> Wagons { get; private set; }

        public Train()
        {
            this.Wagons = new List<Wagon>();
        }

        public void AddAnimals(IEnumerable<Animal> animalCollection)
        {
            IEnumerable<Animal> carnivores = animalCollection
                .Where(a => a.Diet == Diet.Carnivore);
            List<Animal> herbivores = animalCollection
                .Where(animal => animal.Diet == Diet.Herbivore)
                .OrderBy(animal => animal.Weight)
                .ToList();
            foreach (Animal carnivore in carnivores)
            {
                Wagon wagon = new Wagon();
                wagon.Animals.Add(carnivore);
                for (int i = herbivores.Count - 1; i >= 0; i--)
                {
                    if (wagon.IsEligible(herbivores[i]))
                    {
                        wagon.Animals.Add(herbivores[i]);
                        herbivores.RemoveAt(i);
                    }
                }
                this.Wagons.Add(wagon);
            }

            foreach (Animal herbivore in herbivores)
            {
                this.AddAnimal(herbivore);
            }
        }

        private void AddAnimal(Animal animal)
        {
            foreach (Wagon wagon in this.Wagons)
            {
                if (wagon.IsEligible(animal))
                {
                    wagon.Animals.Add(animal);
                    return;
                }
            }
            this.Wagons.Add(new Wagon(animal));
        }

        public override string ToString()
        {
            string rtn = string.Empty;

            foreach (Wagon wagon in Wagons)
            {
                rtn += wagon.ToString() + "\n";
            }

            return rtn;
        }
    }
}
