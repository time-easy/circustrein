﻿using System;
using NUnit.Framework;
using CircustreinApplicatie;

namespace WagonTest
{
    [TestFixture]
    public class UnitTest1
    {
        [Test]
        public void AddToWagon_WhenWagonEmpty_ReturnsTrue()
        {
            Animal animal = new Animal("", Weight.Large, Diet.Carnivore);
            Wagon wagon = new Wagon();

            var result = wagon.IsEligible(animal);

            Assert.IsTrue(result);
        }

        [Test]
        public void AddToWagon_WhenWagonFull_ReturnsFalse()
        {
            Animal animal = new Animal("giraffe", Weight.Large, Diet.Herbivore);
            Wagon wagon = new Wagon();

            wagon.Animals.Add(new Animal
            {
                Name = "test",
                Weight = Weight.Large,
                Diet = Diet.Herbivore
            });
            wagon.Animals.Add(new Animal
            {
                Name = "test",
                Weight = Weight.Large,
                Diet = Diet.Herbivore
            });

            var result = wagon.IsEligible(animal);

            Assert.IsFalse(result);
        }

        [Test]
        public void AddToWagon_WhenWagonContainsCarnivore_ReturnsFalse()
        {
            Animal animal = new Animal("giraffe", Weight.Large, Diet.Herbivore);
            Wagon wagon = new Wagon();

            wagon.Animals.Add(new Animal
            {
                Name = "test",
                Weight = Weight.Large,
                Diet = Diet.Carnivore
            });

            var result = wagon.IsEligible(animal);

            Assert.IsFalse(result);
        }
    }
}
